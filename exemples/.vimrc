" Vim-plug
call plug#begin('~/.vim/plugged')
    " Basic installation
    Plug 'tpope/vim-sensible'

    " Comment/uncomment lines
    Plug 'tpope/vim-commentary'

    " Startify (recent files)
    Plug 'mhinz/vim-startify'

    " Shortcuts
    Plug 'tpope/vim-unimpaired'

    " Drawing tables
    Plug 'vim-scripts/DrawIt'

    " Syntastic
    Plug 'scrooloose/syntastic'

    " Snippets
    Plug 'SirVer/ultisnips'
    Plug 'honza/vim-snippets'

    " Nit
    Plug '~/Applications/nit/misc/vim'

    " Godot
    Plug 'quabug/vim-gdscript'
call plug#end()

" Syntax highlighting
syntax on

" Commentaries
nmap <space>c <Plug>CommentaryLine
xmap <space>c <Plug>Commentary

" Colorscheme
colorscheme desert

" Source the vimrc file after saving it
augroup vimrc
    au!
    au bufwritepost ~/.vim/vimrc source $MYVIMRC
augroup END

" Line numbers
set number

" Replace tab by spaces
set tabstop=4 | set shiftwidth=4 | set expandtab
autocmd filetype tex set tabstop=2 | set shiftwidth=2
autocmd FileType make setlocal noexpandtab

" More than 80 characters
augroup collumnLimit
  autocmd!
  autocmd BufEnter,WinEnter,FileType cpp
        \ highlight CollumnLimit ctermbg=Red guibg=Red
  let collumnLimit = 79 " feel free to customize
  let pattern =
        \ '\%<' . (collumnLimit+1) . 'v.\%>' . collumnLimit . 'v'
  autocmd BufEnter,WinEnter,FileType cpp
        \ let w:m1=matchadd('CollumnLimit', pattern, -1)
augroup END

" Makefile shortcut
nmap mm :!make<CR>
nmap aa :!make all<CR><CR>

" Folding
set foldmethod=indent   
set nofoldenable

" CTRL-R to replace selected text
vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>

